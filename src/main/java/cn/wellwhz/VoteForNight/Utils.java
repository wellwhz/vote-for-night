package cn.wellwhz.VoteForNight;

import java.util.List;

public class Utils {
    public static <E> List<E> addToList(List<E> list,E element){
        list.add(element);
        return list;
    }

    public static <E> List<E> removeFromList(List<E> list,E element){
        list.remove(element);
        return list;
    }
}
