package cn.wellwhz.VoteForNight;

import cn.nukkit.Player;
import cn.nukkit.level.Level;
import cn.nukkit.scheduler.Task;

import java.util.List;

public class WakeUpPlayerTask extends Task {
    private final Player[] playerList;
    private final Level l;
    private final EnterBedEventListener parent;

    public WakeUpPlayerTask(Player[] playerList, Level l,EnterBedEventListener parent) {
        this.playerList = playerList;
        this.l = l;
        this.parent = parent;
    }

    @Override
    public void onRun(int i) {
        for (Player player : playerList) {
            if(!player.isSleeping()){
                return;
            }
        }
        parent.clearLevel(l);
        for (Player player : playerList) {
            player.stopSleep();
        }
        for (Player player : l.getPlayers().values()) {
            player.sendMessage("§a已投票跳过夜晚");
        }
        l.setTime(0);
    }
}
