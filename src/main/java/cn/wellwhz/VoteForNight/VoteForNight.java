package cn.wellwhz.VoteForNight;

import cn.nukkit.plugin.PluginBase;

public class VoteForNight extends PluginBase {
    private static VoteForNight ins;

    public static VoteForNight getInstance() {
        return ins;
    }

    @Override
    public void onLoad() {
        ins = this;
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        registerListeners();
        getLogger().info("§a加载成功！");
        getLogger().info("§a如发现任何bug，请加群859133351反馈！");
    }

    @Override
    public void onDisable() {
    }

    private void registerListeners() {
        this.getServer().getPluginManager().registerEvents(new EnterBedEventListener(), this);
    }
}
